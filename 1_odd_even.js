const arr = [10, 20, 33, 44, 45, 50, 101];

//call evenoddsum function and pass arr as argument
const newarr = evenOddSum(...arr);
console.log(newarr);

//evenOddSum function for sum of numbers
function evenOddSum(...array) {
  const arr = [0, 0];
  for (let i = 0; i < array.length; i++) {
    if (array[i] % 2 === 0) {
      arr[0] += array[i];
    } else {
      arr[1] += array[i];
    }
  }
  return arr;
}
