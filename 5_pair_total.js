const array = [7, 9, 11, 13, 15];
const sum = 20;

console.log(pairwise(array, sum));

function pairwise(array, sum) {
  let indexsum = 0;
  for (let i = 0; i < array.length; i++) {
    for (let j = i; j < array.length; j++) {
      if (array[i] + array[j] === sum) {
        indexsum += i + j;
      }
    }
  }
  return indexsum;
}
